﻿using System;

namespace DomainModel
{
    public abstract class BaseModel
    {
        public int Id { get; set; }
    }
}
