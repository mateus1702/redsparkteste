using System;

namespace DomainModel
{
    public class Consulta : BaseModel
    {
        public DateTime Data { get; set; }

        public Paciente Paciente { get; set; }
        public int PacienteId { get; set; }

        public Medico Medico { get; set; }
        public int MedicoId { get; set; }

        public bool Cancelada { get; set; }
    }
}