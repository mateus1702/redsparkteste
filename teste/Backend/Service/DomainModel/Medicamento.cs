namespace DomainModel
{
    public class Medicamento : BaseModel
    {
        public string Nome { get; set; }

        public Paciente Paciente { get; set; }
        public int PacienteId { get; set; }

    }
}