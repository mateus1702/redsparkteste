using System.Collections.Generic;

namespace DomainModel
{
    public class Medico : Usuario
    {
        public string Especialidade { get; set; }

        public IList<Consulta> Consultas { get; set; }
    }
}