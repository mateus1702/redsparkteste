using System.Collections.Generic;

namespace DomainModel
{
    public class Paciente : Usuario
    {
        public IList<Medicamento> Medicamentos { get; set; }

        public IList<Exame> Exames { get; set; }

        public IList<Consulta> Consultas { get; set; }
    }
}