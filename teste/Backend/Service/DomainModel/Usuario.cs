namespace DomainModel
{
    public class Usuario : BaseModel
    {
        public string Nome { get; set; }

        public string NomeDeUsuario { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public string Tipo { get; set; }

        public bool Desativado { get; set; }
    }
}