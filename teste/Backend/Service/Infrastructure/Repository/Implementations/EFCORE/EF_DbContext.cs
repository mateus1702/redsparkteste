﻿using System;
using Microsoft.EntityFrameworkCore;
using DomainModel;

namespace EFCORE
{
    public class EF_DbContext : DbContext
    {
        public EF_DbContext(DbContextOptions<EF_DbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().HasDiscriminator<string>("Tipo");

            modelBuilder.Entity<Administrador>().HasBaseType<Usuario>();

            modelBuilder.Entity<Medico>()
                .HasBaseType<Usuario>()
                .HasMany(x => x.Consultas)
                .WithOne(x => x.Medico)
                .HasForeignKey(x => x.MedicoId);


            modelBuilder.Entity<Paciente>()
                .HasBaseType<Usuario>()
                .HasMany(x => x.Medicamentos)
                .WithOne(x => x.Paciente)
                .HasForeignKey(x => x.PacienteId);

            modelBuilder.Entity<Paciente>()
                .HasBaseType<Usuario>()
                .HasMany(x => x.Exames)
                .WithOne(x => x.Paciente)
                .HasForeignKey(x => x.PacienteId);

            modelBuilder.Entity<Paciente>()
                .HasBaseType<Usuario>()
                .HasMany(x => x.Consultas)
                .WithOne(x => x.Paciente)
                .HasForeignKey(x => x.PacienteId);


            modelBuilder.Entity<Medicamento>()
                .HasOne(x => x.Paciente)
                .WithMany(x => x.Medicamentos)
                .HasForeignKey(x => x.PacienteId);

            modelBuilder.Entity<Exame>()
                .HasOne(x => x.Paciente)
                .WithMany(x => x.Exames)
                .HasForeignKey(x => x.PacienteId);


            modelBuilder.Entity<Consulta>()
                .HasOne(x => x.Paciente)
                .WithMany(x => x.Consultas)
                .HasForeignKey(x => x.PacienteId);

            modelBuilder.Entity<Consulta>()
                .HasOne(x => x.Medico)
                .WithMany(x => x.Consultas)
                .HasForeignKey(x => x.MedicoId);

            byte[] passwordHash, passwordSalt;
            Security.Cryptography.CreatePasswordHash("123", out passwordHash, out passwordSalt);

            modelBuilder.Entity<Administrador>().HasData(new Administrador()
            {
                Id = 1,
                Nome = "Administrador",
                NomeDeUsuario = "Admin",
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            });

            modelBuilder.Entity<Medico>().HasData(new Medico()
            {
                Id = 2,
                Nome = "Médico",
                NomeDeUsuario = "Medico",
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            });

            modelBuilder.Entity<Paciente>().HasData(new Paciente()
            {
                Id = 3,
                Nome = "Paciente",
                NomeDeUsuario = "Paciente",
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            });
        }
    }
}
