
using DomainModel;
using RepositoryModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EFCORE
{
    public class EF_Repository<TModel> : IRepository<TModel> where TModel : BaseModel
    {
        public EF_Repository(DbContextOptions<EF_DbContext> options)
        {
            _context = new EF_DbContext(options);
        }

        private EF_DbContext _context;

        public int Create(TModel model)
        {
            _context.Set<TModel>().Add(model);
            _context.SaveChanges();
            return model.Id;
        }

        public void Delete(int Id)
        {
            var toDelete = _context.Set<TModel>().SingleAsync(x => x.Id == Id);
            _context.Set<TModel>().Remove(toDelete.Result);
            _context.SaveChanges();
        }

        public TModel Read(int Id)
        {
            return _context.Set<TModel>().Find(Id);
        }

        public IEnumerable<TModel> List()
        {
            return _context.Set<TModel>();
        }

        public IEnumerable<TModel> List(int Page, int QtdPerPage)
        {
            return _context.Set<TModel>()
                .Skip((Page - 1) * QtdPerPage)
                .Take(QtdPerPage);
        }

        public void Update(TModel model)
        {
            _context.Set<TModel>().Attach(model);
            _context.Entry(model).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public int Count()
        {
            return _context.Set<TModel>().Count();
        }

        public Usuario BuscarPorNome(string NomeDeUsuario)
        {
            return _context.Set<Usuario>()
                .FirstOrDefaultAsync(x => x.NomeDeUsuario == NomeDeUsuario)
                .Result;
        }

        public IEnumerable<Consulta> BuscarPorMedicoEData(int MedicoId, DateTime data)
        {
            return _context.Set<Consulta>()
                .Where(x => x.MedicoId == MedicoId && x.Data.Date == data.Date);
        }
    }
}