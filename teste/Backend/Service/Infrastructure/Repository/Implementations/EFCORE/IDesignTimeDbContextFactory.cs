using EFCORE;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace EFCORE
{
    public class IDesignTimeDbContextFactory : IDesignTimeDbContextFactory<EF_DbContext>
    {
        public EF_DbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<EF_DbContext>();
            optionsBuilder.UseSqlite("Data Source=RedSpark.db");

            return new EF_DbContext(optionsBuilder.Options);
        }
    }
}