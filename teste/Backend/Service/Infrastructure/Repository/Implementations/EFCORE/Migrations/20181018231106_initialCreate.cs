﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCORE.Migrations
{
    public partial class initialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    NomeDeUsuario = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<byte[]>(nullable: true),
                    PasswordSalt = table.Column<byte[]>(nullable: true),
                    Tipo = table.Column<string>(nullable: false),
                    Desativado = table.Column<bool>(nullable: false),
                    Especialidade = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Consulta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Data = table.Column<DateTime>(nullable: false),
                    PacienteId = table.Column<int>(nullable: false),
                    MedicoId = table.Column<int>(nullable: false),
                    Cancelada = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Consulta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Consulta_Usuario_MedicoId",
                        column: x => x.MedicoId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Consulta_Usuario_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Exame",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Data = table.Column<DateTime>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    PacienteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exame", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exame_Usuario_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Medicamento",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(nullable: true),
                    PacienteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medicamento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Medicamento_Usuario_PacienteId",
                        column: x => x.PacienteId,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Usuario",
                columns: new[] { "Id", "Desativado", "Nome", "NomeDeUsuario", "PasswordHash", "PasswordSalt", "Tipo" },
                values: new object[] { 1, false, "Administrador", "Admin", new byte[] { 191, 29, 128, 160, 39, 49, 119, 129, 8, 198, 13, 197, 196, 154, 191, 245, 116, 20, 120, 229, 214, 134, 191, 234, 93, 165, 66, 108, 251, 107, 81, 141, 63, 210, 207, 68, 243, 80, 13, 184, 80, 243, 180, 163, 148, 157, 138, 59, 126, 165, 60, 174, 126, 168, 159, 66, 14, 65, 121, 108, 163, 115, 229, 254 }, new byte[] { 199, 157, 100, 56, 98, 16, 75, 245, 153, 177, 11, 8, 153, 115, 39, 188, 165, 13, 7, 214, 201, 80, 101, 203, 117, 11, 177, 71, 111, 218, 75, 93, 0, 233, 240, 206, 165, 162, 71, 153, 169, 32, 2, 144, 70, 247, 8, 169, 124, 250, 241, 25, 105, 231, 20, 149, 131, 243, 180, 97, 76, 83, 255, 35, 94, 114, 186, 202, 121, 84, 248, 7, 91, 36, 65, 130, 148, 57, 115, 18, 135, 220, 222, 34, 5, 39, 159, 1, 50, 53, 161, 207, 251, 79, 128, 180, 236, 54, 165, 66, 231, 146, 209, 206, 84, 220, 61, 130, 152, 127, 42, 15, 95, 200, 41, 8, 240, 183, 27, 143, 127, 36, 154, 39, 252, 80, 222, 246 }, "Administrador" });

            migrationBuilder.InsertData(
                table: "Usuario",
                columns: new[] { "Id", "Desativado", "Nome", "NomeDeUsuario", "PasswordHash", "PasswordSalt", "Tipo", "Especialidade" },
                values: new object[] { 2, false, "Médico", "Medico", new byte[] { 191, 29, 128, 160, 39, 49, 119, 129, 8, 198, 13, 197, 196, 154, 191, 245, 116, 20, 120, 229, 214, 134, 191, 234, 93, 165, 66, 108, 251, 107, 81, 141, 63, 210, 207, 68, 243, 80, 13, 184, 80, 243, 180, 163, 148, 157, 138, 59, 126, 165, 60, 174, 126, 168, 159, 66, 14, 65, 121, 108, 163, 115, 229, 254 }, new byte[] { 199, 157, 100, 56, 98, 16, 75, 245, 153, 177, 11, 8, 153, 115, 39, 188, 165, 13, 7, 214, 201, 80, 101, 203, 117, 11, 177, 71, 111, 218, 75, 93, 0, 233, 240, 206, 165, 162, 71, 153, 169, 32, 2, 144, 70, 247, 8, 169, 124, 250, 241, 25, 105, 231, 20, 149, 131, 243, 180, 97, 76, 83, 255, 35, 94, 114, 186, 202, 121, 84, 248, 7, 91, 36, 65, 130, 148, 57, 115, 18, 135, 220, 222, 34, 5, 39, 159, 1, 50, 53, 161, 207, 251, 79, 128, 180, 236, 54, 165, 66, 231, 146, 209, 206, 84, 220, 61, 130, 152, 127, 42, 15, 95, 200, 41, 8, 240, 183, 27, 143, 127, 36, 154, 39, 252, 80, 222, 246 }, "Medico", null });

            migrationBuilder.InsertData(
                table: "Usuario",
                columns: new[] { "Id", "Desativado", "Nome", "NomeDeUsuario", "PasswordHash", "PasswordSalt", "Tipo" },
                values: new object[] { 3, false, "Paciente", "Paciente", new byte[] { 191, 29, 128, 160, 39, 49, 119, 129, 8, 198, 13, 197, 196, 154, 191, 245, 116, 20, 120, 229, 214, 134, 191, 234, 93, 165, 66, 108, 251, 107, 81, 141, 63, 210, 207, 68, 243, 80, 13, 184, 80, 243, 180, 163, 148, 157, 138, 59, 126, 165, 60, 174, 126, 168, 159, 66, 14, 65, 121, 108, 163, 115, 229, 254 }, new byte[] { 199, 157, 100, 56, 98, 16, 75, 245, 153, 177, 11, 8, 153, 115, 39, 188, 165, 13, 7, 214, 201, 80, 101, 203, 117, 11, 177, 71, 111, 218, 75, 93, 0, 233, 240, 206, 165, 162, 71, 153, 169, 32, 2, 144, 70, 247, 8, 169, 124, 250, 241, 25, 105, 231, 20, 149, 131, 243, 180, 97, 76, 83, 255, 35, 94, 114, 186, 202, 121, 84, 248, 7, 91, 36, 65, 130, 148, 57, 115, 18, 135, 220, 222, 34, 5, 39, 159, 1, 50, 53, 161, 207, 251, 79, 128, 180, 236, 54, 165, 66, 231, 146, 209, 206, 84, 220, 61, 130, 152, 127, 42, 15, 95, 200, 41, 8, 240, 183, 27, 143, 127, 36, 154, 39, 252, 80, 222, 246 }, "Paciente" });

            migrationBuilder.CreateIndex(
                name: "IX_Consulta_MedicoId",
                table: "Consulta",
                column: "MedicoId");

            migrationBuilder.CreateIndex(
                name: "IX_Consulta_PacienteId",
                table: "Consulta",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Exame_PacienteId",
                table: "Exame",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Medicamento_PacienteId",
                table: "Medicamento",
                column: "PacienteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Consulta");

            migrationBuilder.DropTable(
                name: "Exame");

            migrationBuilder.DropTable(
                name: "Medicamento");

            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
