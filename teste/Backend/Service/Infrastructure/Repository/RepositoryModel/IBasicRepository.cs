﻿using System;
using System.Collections.Generic;
using DomainModel;

namespace RepositoryModel
{
    public interface IBasicRepository<TModel> where TModel : BaseModel
    {
        int Create(TModel model);

        void Update(TModel model);

        void Delete(int Id);

        TModel Read(int Id);

        IEnumerable<TModel> List();

        IEnumerable<TModel> List(int Page, int QtdPerPage);

        int Count();
    }
}
