using System;
using System.Collections.Generic;
using DomainModel;

namespace RepositoryModel
{
    public interface IConsultaRepository
    {
        IEnumerable<Consulta> BuscarPorMedicoEData(int MedicoId, DateTime data);
    }
}