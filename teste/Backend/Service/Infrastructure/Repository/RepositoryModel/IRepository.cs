using DomainModel;

namespace RepositoryModel
{
    public interface IRepository<TModel> : IUsuarioRepository, IConsultaRepository, IBasicRepository<TModel> where TModel : BaseModel
    {

    }
}