using DomainModel;

namespace RepositoryModel
{
    public interface IUsuarioRepository
    {
        Usuario BuscarPorNome(string NomeDeUsuario);
    }
}