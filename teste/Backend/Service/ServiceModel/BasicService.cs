using System.Collections.Generic;
using System.Linq;
using DomainModel;
using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel
{
    public class BasicService<TDTO, TModel> : IBasicService<TDTO, TModel> where TModel : BaseModel where TDTO : BaseDTO<TModel>, new()
    {
        public BasicService(IRepository<TModel> repository)
        {
            this._repository = repository;
        }

        protected IRepository<TModel> _repository { get; set; }

        public virtual int Count()
        {
            return _repository.Count();
        }

        public virtual int Create(TDTO dto)
        {
            return _repository.Create(dto.ToModel());
        }

        public virtual void Delete(int Id)
        {
            _repository.Delete(Id);
        }

        public virtual System.Collections.Generic.IEnumerable<TDTO> List()
        {
            return _repository.List().Select(model =>
            {
                var dto = new TDTO();
                dto.FromModel(model);
                return dto;
            });
        }

        public virtual IEnumerable<TDTO> List(int Page, int QtdPerPage)
        {
            return _repository.List(Page, QtdPerPage).Select(model =>
            {
                var dto = new TDTO();
                dto.FromModel(model);
                return dto;
            });
        }

        public virtual TDTO Read(int Id)
        {
            var model = _repository.Read(Id);

            var dto = new TDTO();
            dto.FromModel(model);
            return dto;
        }

        public virtual void Update(TDTO dto)
        {
            _repository.Update(dto.ToModel());
        }
    }
}