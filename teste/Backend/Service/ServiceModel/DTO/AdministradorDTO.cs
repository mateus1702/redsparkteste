using AutoMapper;
using DomainModel;

namespace ServiceModel.DTO
{
    public class AdministradorDTO : BaseDTO<Administrador>
    {
        public AdministradorDTO() { }

        public override Administrador ToModel()
        {
            return _mapper.Map<Administrador>(this);
        }

        public override void FromModel(Administrador model)
        {
            _mapper.Map<Administrador, AdministradorDTO>(model, this);
        }

        public string Nome { get; set; }

        public string NomeDeUsuario { get; set; }

        public string Senha { get; set; }
    }
}