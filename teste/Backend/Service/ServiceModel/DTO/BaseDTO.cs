using AutoMapper;
using DomainModel;

namespace ServiceModel.DTO
{
    public abstract class BaseDTO<TModel> where TModel : BaseModel
    {
        protected IMapper _mapper { get; set; }
        public BaseDTO()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            _mapper = mappingConfig.CreateMapper();
        }

        public int Id { get; set; }

        public abstract TModel ToModel();

        public abstract void FromModel(TModel model);
    }
}