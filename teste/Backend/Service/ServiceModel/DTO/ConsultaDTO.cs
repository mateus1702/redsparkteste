using System;
using DomainModel;

namespace ServiceModel.DTO
{
    public class ConsultaDTO : BaseDTO<Consulta>
    {
        public ConsultaDTO() { }

        public override Consulta ToModel()
        {
            return _mapper.Map<Consulta>(this);
        }

        public override void FromModel(Consulta model)
        {
            _mapper.Map<Consulta, ConsultaDTO>(model, this);
        }

        public DateTime Data { get; set; }

        public int PacienteId { get; set; }

        public int MedicoId { get; set; }
    }
}