using System;
using DomainModel;

namespace ServiceModel.DTO
{
    public class ExameDTO: BaseDTO<Exame>
    {
        public ExameDTO() { }

        public override Exame ToModel()
        {
            return _mapper.Map<Exame>(this);
        }

        public override void FromModel(Exame model)
        {
            _mapper.Map<Exame, ExameDTO>(model, this);
        }

        public string Nome { get; set; }

         public DateTime Data { get; set; }

        public int PacienteId { get; set; }
    }
}