using AutoMapper;
using DomainModel;

namespace ServiceModel.DTO
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Consulta, ConsultaDTO>();
            CreateMap<ConsultaDTO, Consulta>();

            CreateMap<Exame, ExameDTO>();
            CreateMap<ExameDTO, Exame>();

            CreateMap<Medicamento, MedicamentoDTO>();
            CreateMap<MedicamentoDTO, Medicamento>();

            CreateMap<Paciente, PacienteDTO>();
            CreateMap<PacienteDTO, Paciente>();

            CreateMap<Medico, MedicoDTO>();
            CreateMap<MedicoDTO, Medico>();

            CreateMap<Administrador, AdministradorDTO>();
            CreateMap<AdministradorDTO, Administrador>();

            CreateMap<Usuario, UsuarioDTO>();
            CreateMap<UsuarioDTO, Usuario>();
        }
    }
}