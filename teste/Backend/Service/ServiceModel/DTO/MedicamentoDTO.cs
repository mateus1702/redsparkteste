using DomainModel;

namespace ServiceModel.DTO
{
    public class MedicamentoDTO : BaseDTO<Medicamento>
    {
        public MedicamentoDTO() { }

        public override Medicamento ToModel()
        {
            return _mapper.Map<Medicamento>(this);
        }

        public override void FromModel(Medicamento model)
        {
            _mapper.Map<Medicamento, MedicamentoDTO>(model, this);
        }

        public string Nome { get; set; }

        public int PacienteId { get; set; }
    }
}