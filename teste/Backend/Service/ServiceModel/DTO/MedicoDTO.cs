using AutoMapper;
using DomainModel;

namespace ServiceModel.DTO
{
    public class MedicoDTO : BaseDTO<Medico>
    {
        public MedicoDTO() { }

        public override Medico ToModel()
        {
            return _mapper.Map<Medico>(this);
        }

        public override void FromModel(Medico model)
        {
            _mapper.Map<Medico, MedicoDTO>(model, this);
        }

        public string Nome { get; set; }

        public string NomeDeUsuario { get; set; }

        public string Senha { get; set; }

        public string Especialidade { get; set; }

        public bool Desativado { get; set; }
    }
}