using DomainModel;

namespace ServiceModel.DTO
{
    public class PacienteDTO : BaseDTO<Paciente>
    {

        public PacienteDTO() { }

        public override Paciente ToModel()
        {
            return _mapper.Map<Paciente>(this);
        }

        public override void FromModel(Paciente model)
        {
            _mapper.Map<Paciente, PacienteDTO>(model, this);
        }

        public string Nome { get; set; }

        public string NomeDeUsuario { get; set; }

        public string Senha { get; set; }

        public bool Desativado { get; set; }
    }
}