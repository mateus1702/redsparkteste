using AutoMapper;
using DomainModel;

namespace ServiceModel.DTO
{


    public class UsuarioDTO : BaseDTO<Usuario>
    {
        public UsuarioDTO() { }

        public override Usuario ToModel()
        {
            return _mapper.Map<Usuario>(this);
        }

        public override void FromModel(Usuario model)
        {
            _mapper.Map<Usuario, UsuarioDTO>(model, this);
        }

        public string Nome { get; set; }

        public string NomeDeUsuario { get; set; }

        public string Senha { get; set; }

        public string Tipo { get; set; }

        public bool Desativado { get; set; }
    }
}