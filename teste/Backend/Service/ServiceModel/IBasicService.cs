using System.Collections.Generic;
using DomainModel;
using ServiceModel.DTO;

namespace ServiceModel
{
    public interface IBasicService<TDTO, TModel> where TDTO : BaseDTO<TModel> where TModel : BaseModel
    {
        int Create(TDTO dto);

        IEnumerable<TDTO> List();

        IEnumerable<TDTO> List(int Page, int QtdPerPage);

        TDTO Read(int Id);

        void Delete(int Id);

        void Update(TDTO dto);

        int Count();
    }
}