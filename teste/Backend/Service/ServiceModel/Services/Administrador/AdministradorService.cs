using System;
using System.Linq;
using DomainModel;
using RepositoryModel;
using ServiceModel.DTO;
using ServiceModel.Services.Administrador;

namespace ServiceModel.Services
{
    public class AdministradorService : BasicService<AdministradorDTO, DomainModel.Administrador>, IAdministradorService
    {
        public AdministradorService(IRepository<DomainModel.Administrador> usuarioRepository) : base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        private IRepository<DomainModel.Administrador> _usuarioRepository { get; set; }

        public override int Create(AdministradorDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Senha))
                throw new Exception("Senha obrigatória");

            if (_repository.List().Any(x => x.NomeDeUsuario == dto.NomeDeUsuario))
                throw new Exception($"Usuário {dto.NomeDeUsuario} existente.");

            byte[] passwordHash, passwordSalt;
            Security.Cryptography.CreatePasswordHash(dto.Senha, out passwordHash, out passwordSalt);

            var model = new DomainModel.Administrador()
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Nome = dto.Nome,
                NomeDeUsuario = dto.NomeDeUsuario,
            };

            return _repository.Create(model);
        }
    }
}