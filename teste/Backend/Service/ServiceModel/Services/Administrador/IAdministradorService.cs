using ServiceModel.DTO;
using DomainModel;

namespace ServiceModel.Services.Administrador
{
    public interface IAdministradorService : IBasicService<AdministradorDTO, DomainModel.Administrador>
    {
    }
}