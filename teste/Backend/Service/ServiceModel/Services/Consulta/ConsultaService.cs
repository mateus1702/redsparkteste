using System;
using System.Collections.Generic;
using System.Linq;
using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel.Services.Consulta
{
    public class ConsultaService : BasicService<ConsultaDTO, DomainModel.Consulta>, IConsultaService
    {
        public ConsultaService(IRepository<DomainModel.Consulta> repository) : base(repository)
        {
        }

        public override IEnumerable<ConsultaDTO> List()
        {
            return _repository.List().Where(x => !x.Cancelada).Select(x =>
            {
                var dto = new ConsultaDTO();
                dto.FromModel(x);
                return dto;
            });
        }

        public IEnumerable<ConsultaDTO> BuscarPorMedicoEData(int MedicoId, DateTime Data)
        {
            return _repository.BuscarPorMedicoEData(MedicoId, Data).Select(x =>
            {
                var consulta = new ConsultaDTO();
                consulta.FromModel(x);
                return consulta;
            });
        }

        public bool Cancelar(int ConsultaId)
        {
            var consulta = _repository.Read(ConsultaId);

            var horasRestantes = (consulta.Data.Date - DateTime.Now).TotalHours;

            if (horasRestantes < 24)
                return false;

            consulta.Cancelada = true;
            _repository.Update(consulta);
            return true;
        }
    }
}