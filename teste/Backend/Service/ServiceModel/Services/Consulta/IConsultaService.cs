using System;
using System.Collections.Generic;
using ServiceModel.DTO;

namespace ServiceModel.Services.Consulta
{
    public interface IConsultaService : IBasicService<ConsultaDTO, DomainModel.Consulta>
    {
         IEnumerable<ConsultaDTO> BuscarPorMedicoEData(int MedicoId, DateTime data);

         bool Cancelar(int ConsultaId);
    }
}