using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel.Services.Exame
{
    public class ExameService : BasicService<ExameDTO, DomainModel.Exame>, IExameService
    {
        public ExameService(IRepository<DomainModel.Exame> repository) : base(repository)
        {
        }
    }
}