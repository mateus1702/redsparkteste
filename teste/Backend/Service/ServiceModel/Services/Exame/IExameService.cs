using ServiceModel.DTO;

namespace ServiceModel.Services.Exame
{
    public interface IExameService : IBasicService<ExameDTO, DomainModel.Exame>
    {
    }
}