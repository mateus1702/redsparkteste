using ServiceModel.DTO;

namespace ServiceModel.Services.Medicamento
{
    public interface IMedicamentoService: IBasicService<MedicamentoDTO, DomainModel.Medicamento>
    {
    }
}