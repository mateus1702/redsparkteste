using DomainModel;
using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel.Services.Medicamento
{
    public class MedicamentoService : BasicService<MedicamentoDTO, DomainModel.Medicamento>, IMedicamentoService
    {
        public MedicamentoService(IRepository<DomainModel.Medicamento> repository) : base(repository)
        {
        }
    }
}