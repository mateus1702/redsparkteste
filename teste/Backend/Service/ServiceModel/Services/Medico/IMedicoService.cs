using ServiceModel.DTO;
using DomainModel;

namespace ServiceModel.Services.Medico
{
    public interface IMedicoService : IBasicService<MedicoDTO, DomainModel.Medico>
    {
    }
}