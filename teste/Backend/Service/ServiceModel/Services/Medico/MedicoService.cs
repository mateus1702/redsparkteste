using System;
using System.Linq;
using DomainModel;
using RepositoryModel;
using ServiceModel.DTO;
using ServiceModel.Services.Medico;

namespace ServiceModel.Services
{
    public class MedicoService : BasicService<MedicoDTO, DomainModel.Medico>, IMedicoService
    {
        public MedicoService(IRepository<DomainModel.Medico> medicoRepository) : base(medicoRepository)
        {
            _medicoRepository = medicoRepository;
        }

        private IRepository<DomainModel.Medico> _medicoRepository { get; set; }

        public override int Create(MedicoDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Senha))
                throw new Exception("Senha obrigatória");

            if (_repository.List().Any(x => x.NomeDeUsuario == dto.NomeDeUsuario))
                throw new Exception($"Usuário {dto.NomeDeUsuario} existente.");

            byte[] passwordHash, passwordSalt;
            Security.Cryptography.CreatePasswordHash(dto.Senha, out passwordHash, out passwordSalt);

            var model = new DomainModel.Medico()
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Nome = dto.Nome,
                NomeDeUsuario = dto.NomeDeUsuario,
                Especialidade = dto.Especialidade
            };

            return _repository.Create(model);
        }

        public override void Update(MedicoDTO dto)
        {
            var medicoAntigo = _repository.Read(dto.Id);

            medicoAntigo.Nome = dto.Nome;
            medicoAntigo.Desativado = dto.Desativado;
            medicoAntigo.Especialidade = dto.Especialidade;
            
            _repository.Update(medicoAntigo);
        }
    }
}