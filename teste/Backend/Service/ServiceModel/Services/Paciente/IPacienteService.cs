using ServiceModel.DTO;

namespace ServiceModel.Services.Paciente
{
    public interface IPacienteService : IBasicService<PacienteDTO, DomainModel.Paciente>
    {        
    }
}