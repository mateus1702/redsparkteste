using System;
using System.Linq;
using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel.Services.Paciente
{
    public class PacienteService : BasicService<PacienteDTO, DomainModel.Paciente>, IPacienteService
    {
        public PacienteService(IRepository<DomainModel.Paciente> pacienteRepository) : base(pacienteRepository)
        {
            _pacienteRepository = pacienteRepository;
        }

        private IRepository<DomainModel.Paciente> _pacienteRepository { get; set; }
        
        public override int Create(PacienteDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Senha))
                throw new Exception("Senha obrigatória");

            if (_repository.List().Any(x => x.NomeDeUsuario == dto.NomeDeUsuario))
                throw new Exception($"Usuário {dto.NomeDeUsuario} existente.");

            byte[] passwordHash, passwordSalt;
            Security.Cryptography.CreatePasswordHash(dto.Senha, out passwordHash, out passwordSalt);

            var model = new DomainModel.Paciente()
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Nome = dto.Nome,
                NomeDeUsuario = dto.NomeDeUsuario,
            };

            return _repository.Create(model);
        }

        public override void Update(PacienteDTO dto)
        {
            var medicoAntigo = _repository.Read(dto.Id);

            medicoAntigo.Nome = dto.Nome;
            medicoAntigo.Desativado = dto.Desativado;

            _repository.Update(medicoAntigo);
        }
    }
}