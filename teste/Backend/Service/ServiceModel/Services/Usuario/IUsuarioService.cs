using ServiceModel.DTO;
using DomainModel;

namespace ServiceModel.Services.Usuario
{
    public interface IUsuarioService : IBasicService<UsuarioDTO, DomainModel.Usuario>
    {
        UsuarioDTO Autenticar(string NomeDeAdministrador, string Senha);
    }
}