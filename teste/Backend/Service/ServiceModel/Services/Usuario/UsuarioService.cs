using System;
using System.Linq;
using DomainModel;
using RepositoryModel;
using ServiceModel.DTO;

namespace ServiceModel.Services.Usuario
{
    public class UsuarioService : BasicService<UsuarioDTO, DomainModel.Usuario>, IUsuarioService
    {
        public UsuarioService(IRepository<DomainModel.Usuario> usuarioRepository) : base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        private IRepository<DomainModel.Usuario> _usuarioRepository { get; set; }
        public UsuarioDTO Autenticar(string NomeDeUsuario, string Senha)
        {
            var model = _usuarioRepository.BuscarPorNome(NomeDeUsuario);
            if (model == null)
                return null;
            else if (Security.Cryptography.VerifyPasswordHash(Senha, model.PasswordHash, model.PasswordSalt))
            {
                var dto = new UsuarioDTO();
                dto.FromModel(model);
                return dto;
            }
            else
                return null;
        }

    }
}