﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Administrador;

namespace ASPNET_WEBAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomenteAdministradorFilter))]
    public class AdministradorController : ControllerBase
    {
        public AdministradorController(IAdministradorService service)
        {
            this._service = service;
        }

        private IAdministradorService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();

        // GET api/Administrador
        [HttpGet]
        public IEnumerable<AdministradorDTO> Get([FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        // GET api/Administrador/5
        [HttpGet("{id}")]
        public ActionResult<AdministradorDTO> Get(int id) => _service.Read(id);

        // POST api/Administrador
        [HttpPost]
        public void Post([FromBody] AdministradorDTO dto) => _service.Create(dto);

        // PUT api/Administrador/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] AdministradorDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        // DELETE api/Administrador/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
