using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Consulta;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomenteAtivoFilter))]
    public class ConsultaController : ControllerBase
    {
        public ConsultaController(IConsultaService service)
        {
            this._service = service;
        }

        private IConsultaService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();
      
        // GET api/Consulta
        [HttpGet]
        public IEnumerable<ConsultaDTO> Get([FromQuery(Name = "MedicoId")] int? MedicoId, [FromQuery(Name = "Data")] DateTime? Data, [FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (MedicoId.HasValue && Data.HasValue)
                return _service.BuscarPorMedicoEData(MedicoId.Value, Data.Value).ToList();

            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        // GET api/Consulta/5
        [HttpGet("{id}")]
        public ActionResult<ConsultaDTO> Get(int id) => _service.Read(id);

        [ServiceFilter(typeof(SomentePacienteFilter))]
        // POST api/Consulta
        [HttpPost]
        public void Post([FromBody] ConsultaDTO dto) => _service.Create(dto);

        [ServiceFilter(typeof(SomentePacienteFilter))]
        // PUT api/Consulta/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ConsultaDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        [ServiceFilter(typeof(SomentePacienteFilter))]
        // PUT api/Consulta/5
        [HttpPut("cancelar")]
        public ActionResult Canclear([FromBody] ConsultaDTO dto)
        {
            if (_service.Cancelar(dto.Id))
                return Ok();
            else
                return Unauthorized();
        }

        [ServiceFilter(typeof(SomentePacienteFilter))]
        // DELETE api/Consulta/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
