using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Exame;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomentePacienteFilter))]
    [ServiceFilter(typeof(SomenteAtivoFilter))]
    public class ExameController : ControllerBase
    {
        public ExameController(IExameService service)
        {
            this._service = service;
        }

        private IExameService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();

        // GET api/Exame
        [HttpGet]
        public IEnumerable<ExameDTO> Get([FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        // GET api/Exame/5
        [HttpGet("{id}")]
        public ActionResult<ExameDTO> Get(int id) => _service.Read(id);

        // POST api/Exame
        [HttpPost]
        public void Post([FromBody] ExameDTO dto) => _service.Create(dto);

        // PUT api/Exame/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ExameDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        // DELETE api/Exame/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
