using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Medicamento;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomenteMedicoFilter))]
    [ServiceFilter(typeof(SomenteAtivoFilter))]
    public class MedicamentoController : ControllerBase
    {
        public MedicamentoController(IMedicamentoService service)
        {
            this._service = service;
        }

        private IMedicamentoService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();

        // GET api/Medicamento
        [HttpGet]
        public IEnumerable<MedicamentoDTO> Get([FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        // GET api/Medicamento/5
        [HttpGet("{id}")]
        public ActionResult<MedicamentoDTO> Get(int id) => _service.Read(id);

        // POST api/Medicamento
        [HttpPost]
        public void Post([FromBody] MedicamentoDTO dto) => _service.Create(dto);

        // PUT api/Medicamento/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] MedicamentoDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        // DELETE api/Medicamento/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
