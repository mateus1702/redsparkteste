﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Medico;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomenteAtivoFilter))]
    public class MedicoController : ControllerBase
    {
        public MedicoController(IMedicoService service)
        {
            this._service = service;
        }

        private IMedicoService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();

        // GET api/Medico
        [HttpGet]
        public IEnumerable<MedicoDTO> Get([FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        // GET api/Medico/5
        [HttpGet("{id}")]
        public ActionResult<MedicoDTO> Get(int id) => _service.Read(id);

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // POST api/Medico
        [HttpPost]
        public void Post([FromBody] MedicoDTO dto) => _service.Create(dto);

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // PUT api/Medico/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] MedicoDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // DELETE api/Medico/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
