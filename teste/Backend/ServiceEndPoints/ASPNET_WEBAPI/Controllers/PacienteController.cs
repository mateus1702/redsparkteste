using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Paciente;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(SomenteAtivoFilter))]
    public class PacienteController : ControllerBase
    {
        public PacienteController(IPacienteService service)
        {
            this._service = service;
        }

        private IPacienteService _service { get; set; }

        // GET api/Paciente
        [HttpGet("total")]
        public int Total() => _service.Count();

        // GET api/Paciente
        [HttpGet]
        public IEnumerable<PacienteDTO> Get([FromQuery(Name = "Page")] int? Page, [FromQuery(Name = "QtdPerPage")] int? QtdPerPage)
        {
            if (Page.HasValue && QtdPerPage.HasValue)
                return _service.List(Page.Value, QtdPerPage.Value);

            return _service.List().ToList();
        }

        [ServiceFilter(typeof(SomenteAdministradorFilter))]

        // GET api/Paciente/5
        [HttpGet("{id}")]
        public ActionResult<PacienteDTO> Get(int id) => _service.Read(id);

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // POST api/Paciente
        [HttpPost]
        public void Post([FromBody] PacienteDTO dto) => _service.Create(dto);

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // PUT api/Paciente/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] PacienteDTO dto)
        {
            dto.Id = id;
            _service.Update(dto);
        }

        [ServiceFilter(typeof(SomenteAdministradorFilter))]
        // DELETE api/Paciente/5
        [HttpDelete("{id}")]
        public void Delete(int id) => _service.Delete(id);
    }
}
