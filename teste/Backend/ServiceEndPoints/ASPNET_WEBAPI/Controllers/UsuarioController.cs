﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using DomainModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        public UsuarioController(IUsuarioService service, IOptions<AppSettings> appSettings)
        {
            this._service = service;
            this._appSettings = appSettings.Value;
        }

        private IUsuarioService _service { get; set; }

        private readonly AppSettings _appSettings;

        [AllowAnonymous]
        [HttpPost("autenticar")]
        public IActionResult Autenticar([FromBody]UsuarioDTO dto)
        {
            var usuarioAutenticadoDTO = _service.Autenticar(dto.NomeDeUsuario, dto.Senha);

            if (usuarioAutenticadoDTO == null)
                return BadRequest(new { message = "Usuário ou senha incorreta." });

            if (usuarioAutenticadoDTO.Desativado)
                return Unauthorized();


            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuarioAutenticadoDTO.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info (without password) and token to store client side
            return Ok(new
            {
                Usuario = new
                {
                    Id = usuarioAutenticadoDTO.Id,
                    Nome = usuarioAutenticadoDTO.Nome,
                    Tipo = usuarioAutenticadoDTO.Tipo,
                    Token = tokenString,
                }
            });
        }
    }
}
