using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Helpers
{
    public class SomenteAtivoFilter : IActionFilter
    {
        private IUsuarioService _usuarioService { get; set; }

        public SomenteAtivoFilter(IUsuarioService usuarioService)
        {
            this._usuarioService = usuarioService;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var usuario = _usuarioService.Read(int.Parse(context.HttpContext.User.Identity.Name));
            if (usuario.Desativado)
                context.Result = new UnauthorizedResult();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}