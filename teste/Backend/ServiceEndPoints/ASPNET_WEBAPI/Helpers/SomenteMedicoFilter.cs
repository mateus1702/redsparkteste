using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI.Helpers
{
    public class SomenteMedicoFilter : IActionFilter
    {
        private IUsuarioService _usuarioService { get; set; }

        public SomenteMedicoFilter(IUsuarioService usuarioService)
        {
            this._usuarioService = usuarioService;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var usuario = _usuarioService.Read(int.Parse(context.HttpContext.User.Identity.Name));
            if (usuario.Tipo != "Medico")
                context.Result = new UnauthorizedResult();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}