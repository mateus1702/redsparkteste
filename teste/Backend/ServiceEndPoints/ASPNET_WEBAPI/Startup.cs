﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASPNET_WEBAPI.Helpers;
using AutoMapper;
using DomainModel;
using EFCORE;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using RepositoryModel;
using ServiceModel.DTO;
using ServiceModel.Services;
using ServiceModel.Services.Administrador;
using ServiceModel.Services.Consulta;
using ServiceModel.Services.Exame;
using ServiceModel.Services.Medicamento;
using ServiceModel.Services.Medico;
using ServiceModel.Services.Paciente;
using ServiceModel.Services.Usuario;

namespace ASPNET_WEBAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var connectionString = Configuration.GetConnectionString("DbContext");

            var optionsBuilder = new DbContextOptionsBuilder<EF_DbContext>();
            optionsBuilder.UseSqlite(connectionString);

            //Repositorios
            services.AddScoped<IRepository<Usuario>>(x =>
            {
                return new EF_Repository<Usuario>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Administrador>>(x =>
            {
                return new EF_Repository<Administrador>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Medico>>(x =>
            {
                return new EF_Repository<Medico>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Paciente>>(x =>
            {
                return new EF_Repository<Paciente>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Medicamento>>(x =>
            {
                return new EF_Repository<Medicamento>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Exame>>(x =>
            {
                return new EF_Repository<Exame>(optionsBuilder.Options);
            });

            services.AddScoped<IRepository<Consulta>>(x =>
            {
                return new EF_Repository<Consulta>(optionsBuilder.Options);
            });


            //Servicos
            services.AddScoped<IUsuarioService, UsuarioService>();

            services.AddScoped<IAdministradorService, AdministradorService>();

            services.AddScoped<IMedicoService, MedicoService>();

            services.AddScoped<IPacienteService, PacienteService>();

            services.AddScoped<IMedicamentoService, MedicamentoService>();

            services.AddScoped<IExameService, ExameService>();

            services.AddScoped<IConsultaService, ConsultaService>();


            //Service filters
            services.AddScoped<SomenteAdministradorFilter>();

            services.AddScoped<SomenteMedicoFilter>();

            services.AddScoped<SomentePacienteFilter>();

            services.AddScoped<SomenteAtivoFilter>();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            //Setup CORS
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowCredentials();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins", corsBuilder.Build());
            });

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var usuarioService = context.HttpContext.RequestServices.GetRequiredService<IUsuarioService>();
                        var usuarioId = int.Parse(context.Principal.Identity.Name);
                        var usuario = usuarioService.Read(usuarioId);
                        if (usuario == null)
                        {
                            context.Fail("Unauthorized");
                        }
                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("AllowAllOrigins");

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
