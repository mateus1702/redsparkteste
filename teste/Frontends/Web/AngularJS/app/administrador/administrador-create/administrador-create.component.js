'use strict';

angular.
  module('administrador').
  component('administradorCreate', {
     templateUrl: '/administrador/administrador-create/administrador-create.template.html',
     controller: ['$scope', 'Administrador', '$location',
       function AdministradorCreateController($scope, Administrador, $location) {
         $scope.administrador = new Administrador();
         $scope.administrador.validation = {
           nomeRequiredOk: true,           
           nomeDeUsuarioRequiredOk: true,    
           senhaRequiredOk: true,    
         };

         $scope.create = function () {
           $scope.administrador.validation.nomeRequiredOk = !$scope.administrador.Nome ? false : true;           
           $scope.administrador.validation.nomeDeUsuarioRequiredOk = !$scope.administrador.NomeDeUsuario ? false : true;  
           $scope.administrador.validation.senhaRequiredOk = !$scope.administrador.Senha ? false : true;  

           var isValid = true;
           angular.forEach($scope.administrador.validation, function(value, key) {
            isValid = isValid && value;
           });

           if (isValid) {
             $scope.loading = true;
             Administrador.save($scope.administrador, function() {
               $scope.loading = false;
               $location.path( "/administrador" );
             });
           }
         }
       }
     ]
  });
