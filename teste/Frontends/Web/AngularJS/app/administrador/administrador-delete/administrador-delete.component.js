'use strict';

angular.
  module('administrador').
  component('administradorDelete', {
     templateUrl: '/administrador/administrador-delete/administrador-delete.template.html',
     controller: ['$scope', 'Administrador', '$routeParams', '$location',
       function AdministradorDeleteController($scope, Administrador, $routeParams, $location) {
         $scope.loading = true;
         $scope.administrador = Administrador.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });

         $scope.delete = function () {
           $scope.loading = true;
           $scope.administrador.$delete(function() {
             $scope.loading = false;
             $location.path( "/administrador" );
           });
         }
       }
     ]
  });
