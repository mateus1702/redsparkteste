'use strict';

angular.
  module('administrador').
  component('administradorList', {
    templateUrl: '/administrador/administrador-list/administrador-list.template.html',
    controller: ['$rootScope', '$scope', 'Administrador', 'Paging', 'AdministradorTotal',
      function AdministradorListController($rootScope, $scope, Administrador, Paging, AdministradorTotal) {
        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.administradores = Administrador.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        AdministradorTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });
      }
    ]
  });
