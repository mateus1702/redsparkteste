'use strict';

angular.
  module('administrador').
  component('administradorRead', {
     templateUrl: '/administrador/administrador-read/administrador-read.template.html',
     controller: ['Administrador', '$routeParams', '$scope',
       function AdministradorReadController(Administrador, $routeParams, $scope) {
         $scope.Id = $routeParams.Id;
         $scope.loading = true;
         $scope.administrador = Administrador.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });
       }
     ]
  });
