'use strict';

angular.
  module('administrador').
  component('administradorUpdate', {
     templateUrl: '/administrador/administrador-update/administrador-update.template.html',
     controller: ['$scope', 'Administrador', '$routeParams' ,'$location',
       function AdministradorUpdateController($scope, Administrador, $routeParams, $location) {
         $scope.loading = true;
         $scope.administrador = Administrador.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
           $scope.administrador.validation = {
             nomeRequiredOk: true
           };
         });

         $scope.update = function () {
           $scope.administrador.validation.nomeRequiredOk = !$scope.administrador.Nome ? false : true;
           
           var isValid = true;
           angular.forEach($scope.administrador.validation, function(value, key) {
            isValid = isValid && value;
           });

           if (isValid) {
             $scope.loading = true;
             $scope.administrador.$update(function() {
               $scope.loading = false;
               $location.path( "/administrador" );
             });
           }
         }
       }
     ]
  });
