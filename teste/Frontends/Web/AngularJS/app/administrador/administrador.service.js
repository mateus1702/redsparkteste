'use strict';

angular.
  module('administrador').
  factory('Administrador', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/administrador/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);


angular.
  module('administrador').
  factory('AdministradorTotal', ['$http',
    function ($http) {
      return function () {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/administrador/total'
        })
      }
    }
  ]);
