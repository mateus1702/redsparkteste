'use strict';

angular.
  module('angularJSUIApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/usuario/login', {
          template: '<usuario-login></usuario-login>'
        }).       
        when('/administrador', {
          template: '<administrador-list></administrador-list>'
        }).
        when('/administrador/create', {
          template: '<administrador-create></administrador-create>'
        }).
        when('/administrador/read/:Id', {
          template: '<administrador-read></administrador-read>'
        }).
        when('/administrador/update/:Id', {
          template: '<administrador-update></administrador-update>'
        }).
        when('/administrador/delete/:Id', {
          template: '<administrador-delete></administrador-delete>'
        }).
        when('/medico', {
          template: '<medico-list></medico-list>'
        }).
        when('/medico/create', {
          template: '<medico-create></medico-create>'
        }).
        when('/medico/read/:Id', {
          template: '<medico-read></medico-read>'
        }).
        when('/medico/update/:Id', {
          template: '<medico-update></medico-update>'
        }).
        when('/medico/delete/:Id', {
          template: '<medico-delete></medico-delete>'
        }).
        when('/paciente', {
          template: '<paciente-list></paciente-list>'
        }).
        when('/paciente/create', {
          template: '<paciente-create></paciente-create>'
        }).
        when('/paciente/read/:Id', {
          template: '<paciente-read></paciente-read>'
        }).
        when('/paciente/update/:Id', {
          template: '<paciente-update></paciente-update>'
        }).
        when('/paciente/delete/:Id', {
          template: '<paciente-delete></paciente-delete>'
        }).
        when('/medicamento', {
          template: '<medicamento-list></medicamento-list>'
        }).
        when('/medicamento/create', {
          template: '<medicamento-create></medicamento-create>'
        }).
        when('/medicamento/read/:Id', {
          template: '<medicamento-read></medicamento-read>'
        }).
        when('/medicamento/update/:Id', {
          template: '<medicamento-update></medicamento-update>'
        }).
        when('/medicamento/delete/:Id', {
          template: '<medicamento-delete></medicamento-delete>'
        }).
        when('/exame', {
          template: '<exame-list></exame-list>'
        }).
        when('/exame/create', {
          template: '<exame-create></exame-create>'
        }).
        when('/exame/read/:Id', {
          template: '<exame-read></exame-read>'
        }).
        when('/exame/update/:Id', {
          template: '<exame-update></exame-update>'
        }).
        when('/exame/delete/:Id', {
          template: '<exame-delete></exame-delete>'
        }).
        when('/consulta', {
          template: '<consulta-list></consulta-list>'
        }).
        when('/consulta/create', {
          template: '<consulta-create></consulta-create>'
        }).
        when('/consulta/read/:Id', {
          template: '<consulta-read></consulta-read>'
        }).
        when('/consulta/update/:Id', {
          template: '<consulta-update></consulta-update>'
        }).
        when('/consulta/delete/:Id', {
          template: '<consulta-delete></consulta-delete>'
        }).
        when('/consulta/cancelar/:Id', {
          template: '<consulta-cancelar></consulta-cancelar>'
        }).
        when('/consulta/listbydate', {
          template: '<consulta-list-by-date></consulta-list-by-date>'
        }).
        when('/home', {
          template: '<home></home>'
        }).
        otherwise('/home');
    }
  ]);
