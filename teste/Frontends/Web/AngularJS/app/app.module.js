'use strict';

angular.module('angularJSUIApp', [
  'common',
  'home',
  'ngRoute',
  'usuario',
  'administrador',
  'medico',
  'paciente',
  'medicamento',
  'exame',
  'consulta'
]);