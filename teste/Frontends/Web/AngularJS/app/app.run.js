'use strict';

angular.module('angularJSUIApp')
  .run(['$rootScope', '$location', function ($rootScope, $location, Auth) {
    $rootScope.qtdPerPage = 2;
    $rootScope.$on('$routeChangeStart', function (event) {
      if ($location.$$path != "/usuario/login") {
        if (!$rootScope.Usuario || !$rootScope.Usuario.Token) {
          console.log('UNAUTHORIZED');
          event.preventDefault();
          $location.path('/usuario/login');
        }
      }
    });
  }]);