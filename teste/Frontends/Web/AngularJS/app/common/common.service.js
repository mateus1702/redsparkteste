'use strict';

angular.
  module('common').
  factory('Paging', ['$resource',
    function () {
      return function (loadPage) {
        var paging = {
          page: 1,
          qtdPerPage: 0,
          pages: [],
          totalRecords: 0,
          totalPages: 0,
          init: function (totalRecords, qtdPerPage) {
            paging.totalRecords = totalRecords;
            paging.qtdPerPage = qtdPerPage;
            paging.totalPages = Math.floor(paging.totalRecords / paging.qtdPerPage);
            var remainer = paging.totalRecords % paging.qtdPerPage;
            if (remainer > 0)
              paging.totalPages = paging.totalPages + 1;

            paging.pages = [];
            for (var i = 1; i <= paging.totalPages; i++) {
              paging.pages.push({
                number: i,
                selected: i == 1
              });
            }

            paging.loadPage(paging.page, paging.qtdPerPage);
          },
          loadPage: loadPage,
          changePage: function (number) {
            paging.page = number;
            angular.forEach(paging.pages, function (page) {
              if (page.number === paging.page)
                page.selected = true;
              else
                page.selected = false;
            });
            paging.loadPage(paging.page, paging.qtdPerPage);
          }
        };

        return paging;
      }
    }
  ]);
