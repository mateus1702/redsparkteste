'use strict'

angular.
  module('common').
  component('header', {
    templateUrl: '/common/header/header.template.html',
    controller: ['$rootScope', '$scope',
      function HeaderController($rootScope, $scope) {
        $scope.collapse = false;

        $scope.toggleCollapse = function () {
          $scope.collapse = !$scope.collapse;
        }
        
        $scope.Autenticado = function() {
          return $rootScope.Usuario;
        }

        $scope.TipoUsuario = function() {
          return $rootScope.Usuario ? $rootScope.Usuario.Tipo : '';
        }
      }
    ]
  });
