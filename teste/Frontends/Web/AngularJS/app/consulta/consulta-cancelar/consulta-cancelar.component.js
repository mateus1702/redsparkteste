'use strict';

angular.
  module('consulta').
  component('consultaCancelar', {
    templateUrl: '/consulta/consulta-cancelar/consulta-cancelar.template.html',
    controller: ['$scope', 'Consulta', '$routeParams', '$location', 'Medico', 'ConsultaCancelar',
      function ConsultaCancelarController($scope, Consulta, $routeParams, $location, Medico, ConsultaCancelar) {
        $scope.loading = true;
        $scope.consulta = Consulta.get({ Id: $routeParams.Id }, function (response) {
          $scope.medico = Medico.get({ Id: $scope.consulta.MedicoId }, function (response) {
            $scope.consulta.Medico = $scope.medico;
            $scope.loading = false;
          });
        });

        $scope.cancel = function () {
          $scope.loading = true;
          ConsultaCancelar($scope.consulta.Id)
            .then(function successCallback(response) {
              $scope.loading = false;
              $location.path("/consulta");
            }, function errorCallback(response) {
              $scope.loading = false;
              if (response.status === 401) {
                alert('Não autorizado.')
              } else {
                alert(response.statusText);
              }
            });
        }
      }
    ]
  });
