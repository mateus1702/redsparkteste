'use strict';

angular.
  module('consulta').
  component('consultaCreate', {
    templateUrl: '/consulta/consulta-create/consulta-create.template.html',
    controller: ['$rootScope', '$scope', 'Consulta', '$location', 'Medico',
      function ConsultaCreateController($rootScope, $scope, Consulta, $location, Medico) {
        $scope.loading = true;
        $scope.medicos = Medico.query(function () {
          $scope.loading = false;
        });

        $scope.consulta = new Consulta();
        $scope.consulta.validation = {
          dataRequiredOk: true,
          medicoRequiredOk: true,
        };

        $scope.create = function () {
          $scope.consulta.validation.dataRequiredOk = !$scope.consulta.Data ? false : true;
          $scope.consulta.validation.medicoRequiredOk = !$scope.consulta.Medico ? false : true;

          var isValid = true;
          angular.forEach($scope.consulta.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.consulta.PacienteId = $rootScope.Usuario.Id;
            $scope.consulta.MedicoId = $scope.consulta.Medico.Id;

            $scope.loading = true;
            Consulta.save($scope.consulta, function () {
              $scope.loading = false;
              $location.path("/consulta");
            });
          }
        }
      }
    ]
  });
