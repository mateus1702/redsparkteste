'use strict';

angular.
  module('consulta').
  component('consultaDelete', {
    templateUrl: '/consulta/consulta-delete/consulta-delete.template.html',
    controller: ['$scope', 'Consulta', '$routeParams', '$location', 'Medico',
      function ConsultaDeleteController($scope, Consulta, $routeParams, $location, Medico) {
        $scope.loading = true;
        $scope.consulta = Consulta.get({ Id: $routeParams.Id }, function (response) {
          $scope.medico = Medico.get({ Id: $scope.consulta.MedicoId }, function (response) {
            $scope.consulta.Medico = $scope.medico;
            $scope.loading = false;
          });
        });

        $scope.delete = function () {
          $scope.loading = true;
          $scope.consulta.$delete(function () {
            $scope.loading = false;
            $location.path("/consulta");
          });
        }
      }
    ]
  });
