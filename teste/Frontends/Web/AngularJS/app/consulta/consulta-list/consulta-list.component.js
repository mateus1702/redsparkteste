'use strict';

angular.
  module('consulta').
  component('consultaList', {
    templateUrl: '/consulta/consulta-list/consulta-list.template.html',
    controller: ['$scope', 'Consulta', 'Medico', 'ConsultaTotal', 'Paging', '$rootScope',
      function ConsultaListController($scope, Consulta, Medico, ConsultaTotal, Paging, $rootScope) {
        $scope.loading = true;
        $scope.medicos = Medico.query(function () {
          $scope.loading = false;
        });

        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.consultas = Consulta.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        ConsultaTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });

        $scope.BuscarNomeMedico = function (MedicoId) {
          var nome = "Não encontrado.";
          angular.forEach($scope.medicos, function (Medico) {
            if (Medico.Id === MedicoId) {
              nome = Medico.Nome;
            }
          });
          return nome;
        }
      }
    ]
  });
