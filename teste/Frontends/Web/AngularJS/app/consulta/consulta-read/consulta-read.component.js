'use strict';

angular.
  module('consulta').
  component('consultaRead', {
    templateUrl: '/consulta/consulta-read/consulta-read.template.html',
    controller: ['Consulta', '$routeParams', '$scope', 'Medico',
      function ConsultaReadController(Consulta, $routeParams, $scope, Medico) {
        $scope.Id = $routeParams.Id;

        $scope.loading = true;
        $scope.consulta = Consulta.get({ Id: $routeParams.Id }, function (response) {
          $scope.medico = Medico.get({ Id: $scope.consulta.MedicoId }, function (response) {
            $scope.consulta.Medico = $scope.medico;
            $scope.loading = false;
          });
        });
      }
    ]
  });
