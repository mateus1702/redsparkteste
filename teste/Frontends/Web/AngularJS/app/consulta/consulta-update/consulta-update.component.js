'use strict';

angular.
  module('consulta').
  component('consultaUpdate', {
    templateUrl: '/consulta/consulta-update/consulta-update.template.html',
    controller: ['$rootScope', '$scope', 'Consulta', '$routeParams', '$location', 'Medico',
      function ConsultaUpdateController($rootScope, $scope, Consulta, $routeParams, $location, Medico) {
        $scope.loading = true;
        $scope.consulta = Consulta.get({ Id: $routeParams.Id }, function (response) {
          $scope.medicos = Medico.query(function () {
            angular.forEach($scope.medicos,function(Medico) {
              if(Medico.Id == $scope.consulta.MedicoId){
                $scope.consulta.Medico = Medico;
              }
            });            
            $scope.consulta.Data = new Date(response.Data);
            $scope.consulta.validation = {
              dataRequiredOk: true,
              medicoRequiredOk: true
            };
            $scope.loading = false;
          });
        });

        $scope.update = function () {
          $scope.consulta.validation.dataRequiredOk = !$scope.consulta.Data ? false : true;
          $scope.consulta.validation.medicoRequiredOk = !$scope.consulta.Medico ? false : true;

          var isValid = true;
          angular.forEach($scope.consulta.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.consulta.PacienteId = $rootScope.Usuario.Id;
            $scope.consulta.MedicoId = $scope.consulta.Medico.Id;

            $scope.loading = true;
            $scope.consulta.$update(function () {
              $scope.loading = false;
              $location.path("/consulta");
            });
          }
        }


      }
    ]
  });
