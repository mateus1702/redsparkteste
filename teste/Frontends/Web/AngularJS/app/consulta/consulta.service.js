'use strict';

angular.
  module('consulta').
  factory('Consulta', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/consulta/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);

angular.
  module('consulta').
  factory('ConsultaTotal', ['$http',
    function ($http) {
      return function (ConsultaId) {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/consulta/total'
        })
      }
    }
  ]);

angular.
  module('consulta').
  factory('ConsultaCancelar', ['$http',
    function ($http) {
      return function (ConsultaId) {
        return $http({
          method: 'PUT',
          data: {
            Id: ConsultaId
          },
          url: 'http://localhost:5000/api/consulta/cancelar'
        })
      }
    }
  ]);
