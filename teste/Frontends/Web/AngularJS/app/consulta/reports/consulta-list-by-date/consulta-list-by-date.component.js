'use strict';

angular.
  module('consulta').
  component('consultaListByDate', {
    templateUrl: '/consulta/reports/consulta-list-by-date/consulta-list-by-date.template.html',
    controller: ['$rootScope', '$scope', 'Consulta', 'Paciente',
      function consultaListByDateController($rootScope, $scope, Consulta, Paciente) {
        $scope.parametros = {
        };

        $scope.parametros.Data = null;
        $scope.parametros.validation = {
          dataRequiredOk: true,
        };

        $scope.search = function () {
          $scope.parametros.validation.dataRequiredOk = !$scope.parametros.Data ? false : true;

          var isValid = true;
          angular.forEach($scope.parametros.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            $scope.pacientes = Paciente.query(function () {
              $scope.consultas = Consulta.query({ MedicoId: $rootScope.Usuario.Id, Data: $scope.parametros.Data }, function () {
                $scope.loading = false;
              });
            });
          }
        };

        $scope.BuscarNomePaciente = function (PacienteId) {
          var nome = "Não encontrado.";
          angular.forEach($scope.pacientes, function (Paciente) {
            if (Paciente.Id === PacienteId) {
              nome = Paciente.Nome;
            }
          });
          return nome;
        }
      }
    ]
  });
