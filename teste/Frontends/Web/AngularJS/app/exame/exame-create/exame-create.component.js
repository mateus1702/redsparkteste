'use strict';

angular.
  module('exame').
  component('exameCreate', {
    templateUrl: '/exame/exame-create/exame-create.template.html',
    controller: ['$rootScope', '$scope', 'Exame', '$location',
      function ExameCreateController($rootScope, $scope, Exame, $location) {
        $scope.exame = new Exame();
        $scope.exame.validation = {
          nomeRequiredOk: true,
          dataRequiredOk: true,
        };

        $scope.create = function () {
          $scope.exame.validation.nomeRequiredOk = !$scope.exame.Nome ? false : true;
          $scope.exame.validation.dataRequiredOk = !$scope.exame.Data ? false : true;

          var isValid = true;
          angular.forEach($scope.exame.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.exame.PacienteId = $rootScope.Usuario.Id;
            $scope.loading = true;
            Exame.save($scope.exame, function () {
              $scope.loading = false;
              $location.path("/exame");
            });
          }
        }
      }
    ]
  });
