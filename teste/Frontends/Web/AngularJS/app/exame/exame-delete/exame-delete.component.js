'use strict';

angular.
  module('exame').
  component('exameDelete', {
    templateUrl: '/exame/exame-delete/exame-delete.template.html',
    controller: ['$scope', 'Exame', '$routeParams', '$location', 'Paciente',
      function ExameDeleteController($scope, Exame, $routeParams, $location, Paciente) {
        $scope.loading = true;
        $scope.exame = Exame.get({ Id: $routeParams.Id }, function (response) {
          $scope.loading = false;
        });

        $scope.delete = function () {
          $scope.loading = true;
          $scope.exame.$delete(function () {
            $scope.loading = false;
            $location.path("/exame");
          });
        }
      }
    ]
  });
