'use strict';

angular.
  module('exame').
  component('exameList', {
    templateUrl: '/exame/exame-list/exame-list.template.html',
    controller: ['$scope', 'Exame', 'Paciente', 'Paging', 'ExameTotal', '$rootScope',
      function ExameListController($scope, Exame, Paciente, Paging, ExameTotal, $rootScope) {    
        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.exames = Exame.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        ExameTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });
      }
    ]
  });
