'use strict';

angular.
  module('exame').
  component('exameRead', {
    templateUrl: '/exame/exame-read/exame-read.template.html',
    controller: ['Exame', '$routeParams', '$scope',
      function ExameReadController(Exame, $routeParams, $scope) {
        $scope.Id = $routeParams.Id;

        $scope.loading = true;
        $scope.exame = Exame.get({ Id: $routeParams.Id }, function (response) {
          $scope.loading = false;
        });
      }
    ]
  });
