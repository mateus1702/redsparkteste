'use strict';

angular.
  module('exame').
  component('exameUpdate', {
    templateUrl: '/exame/exame-update/exame-update.template.html',
    controller: ['$rootScope', '$scope', 'Exame', '$routeParams', '$location',
      function ExameUpdateController($rootScope, $scope, Exame, $routeParams, $location) {
        $scope.loading = true;
        $scope.exame = Exame.get({ Id: $routeParams.Id }, function (response) {
          $scope.loading = false;
          $scope.exame.Data = new Date(response.Data);
          $scope.exame.validation = {
            nomeRequiredOk: true,
            dataRequiredOk: true,
          };
        });


        $scope.update = function () {
          $scope.exame.validation.nomeRequiredOk = !$scope.exame.Nome ? false : true;
          $scope.exame.validation.dataRequiredOk = !$scope.exame.Data ? false : true;

          var isValid = true;
          angular.forEach($scope.exame.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.exame.PacienteId = $rootScope.Usuario.Id;
            $scope.loading = true;
            $scope.exame.$update(function () {
              $scope.loading = false;
              $location.path("/exame");
            });
          }
        }


      }
    ]
  });
