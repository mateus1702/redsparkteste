'use strict';

angular.
  module('exame').
  factory('Exame', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/exame/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);



angular.
  module('exame').
  factory('ExameTotal', ['$http',
    function ($http) {
      return function (ConsultaId) {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/exame/total'
        });
      }
    }
  ]);

