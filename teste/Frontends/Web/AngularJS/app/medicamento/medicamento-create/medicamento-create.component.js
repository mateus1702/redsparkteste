'use strict';

angular.
  module('medicamento').
  component('medicamentoCreate', {
    templateUrl: '/medicamento/medicamento-create/medicamento-create.template.html',
    controller: ['$scope', 'Medicamento', 'Paciente', '$location',
      function MedicamentoCreateController($scope, Medicamento, Paciente, $location) {
        $scope.loading = true;
        $scope.pacientes = Paciente.query(function () {
          $scope.loading = false;
        });
        
        $scope.medicamento = new Medicamento();
        $scope.medicamento.validation = {
          nomeRequiredOk: true,
          pacienteRequiredOk: true,
        };

        $scope.create = function () {
          $scope.medicamento.validation.nomeRequiredOk = !$scope.medicamento.Nome ? false : true;
          $scope.medicamento.validation.pacienteRequiredOk = !$scope.medicamento.Paciente ? false : true;

          var isValid = true;
          angular.forEach($scope.medicamento.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            $scope.medicamento.PacienteId = $scope.medicamento.Paciente.Id;
            Medicamento.save($scope.medicamento, function () {
              $scope.loading = false;
              $location.path("/medicamento");
            });
          }
        }

       
      }
    ]
  });
