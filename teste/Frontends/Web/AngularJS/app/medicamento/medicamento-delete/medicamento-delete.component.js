'use strict';

angular.
  module('medicamento').
  component('medicamentoDelete', {
    templateUrl: '/medicamento/medicamento-delete/medicamento-delete.template.html',
    controller: ['$scope', 'Medicamento', '$routeParams', '$location', 'Paciente',
      function MedicamentoDeleteController($scope, Medicamento, $routeParams, $location, Paciente) {        
        $scope.loading = true;
        $scope.pacientes = Paciente.query(function () {
          $scope.medicamento = Medicamento.get({ Id: $routeParams.Id }, function (response) {
            angular.forEach($scope.pacientes, function (paciente, index) {
              if (paciente.Id == $scope.medicamento.PacienteId) {
                $scope.medicamento.Paciente = paciente;
              }
            });
            $scope.loading = false;
          });
        });

        $scope.delete = function () {
          $scope.loading = true;
          $scope.medicamento.$delete(function () {
            $scope.loading = false;
            $location.path("/medicamento");
          });
        }
      }
    ]
  });
