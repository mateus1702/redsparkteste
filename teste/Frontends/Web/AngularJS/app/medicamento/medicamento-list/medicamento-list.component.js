'use strict';

angular.
  module('medicamento').
  component('medicamentoList', {
    templateUrl: '/medicamento/medicamento-list/medicamento-list.template.html',
    controller: ['$scope', 'Medicamento', 'MedicamentoTotal', '$rootScope', 'Paging',
      function MedicamentoListController($scope, Medicamento, MedicamentoTotal, $rootScope, Paging) {
        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.medicamentos = Medicamento.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        MedicamentoTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });

        $scope.BuscarNomePaciente = function (PacienteId) {
          var nome = 'Não encontrado';
          angular.forEach($scope.pacientes, function (paciente) {
            if (paciente.Id === PacienteId) {
              nome = paciente.Nome;
            }
          });
          return nome;
        }
      }
    ]
  });
