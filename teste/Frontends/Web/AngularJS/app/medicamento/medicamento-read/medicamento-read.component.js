'use strict';

angular.
  module('medicamento').
  component('medicamentoRead', {
    templateUrl: '/medicamento/medicamento-read/medicamento-read.template.html',
    controller: ['Medicamento', '$routeParams', '$scope', 'Paciente',
      function MedicamentoReadController(Medicamento, $routeParams, $scope, Paciente) {
        $scope.Id = $routeParams.Id;

        $scope.loading = true;
        $scope.pacientes = Paciente.query(function () {
          $scope.medicamento = Medicamento.get({ Id: $routeParams.Id }, function (response) {
            angular.forEach($scope.pacientes, function(paciente, index) {
              if(paciente.Id == $scope.medicamento.PacienteId){
                $scope.medicamento.Paciente = paciente;
              }
            });
            $scope.loading = false;
          });
        });
      }
    ]
  });
