'use strict';

angular.
  module('medicamento').
  component('medicamentoUpdate', {
    templateUrl: '/medicamento/medicamento-update/medicamento-update.template.html',
    controller: ['$scope', 'Medicamento', '$routeParams', '$location', 'Paciente',
      function MedicamentoUpdateController($scope, Medicamento, $routeParams, $location, Paciente) {
        $scope.loading = true;
        $scope.pacientes = Paciente.query(function () {
          $scope.medicamento = Medicamento.get({ Id: $routeParams.Id }, function () {
            angular.forEach($scope.pacientes, function (paciente) {
              if (paciente.Id == $scope.medicamento.PacienteId) {
                $scope.medicamento.Paciente = paciente;
              }
            });
            $scope.medicamento.validation = {
              nomeRequiredOk: true,
              pacienteRequiredOk: true,
            };

            $scope.loading = false;
          });
        });


        $scope.update = function () {
          $scope.medicamento.validation.nomeRequiredOk = !$scope.medicamento.Nome ? false : true;
          $scope.medicamento.validation.pacienteRequiredOk = !$scope.medicamento.Paciente ? false : true;

          var isValid = true;
          angular.forEach($scope.medicamento.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            $scope.medicamento.PacienteId = $scope.medicamento.Paciente.Id;
            $scope.medicamento.$update(function () {
              $scope.loading = false;
              $location.path("/medicamento");
            });
          }
        }


      }
    ]
  });
