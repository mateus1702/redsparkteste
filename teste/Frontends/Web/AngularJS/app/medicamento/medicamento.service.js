'use strict';

angular.
  module('medicamento').
  factory('Medicamento', ['$resource',
    function($resource) {
      return $resource('http://localhost:5000/api/medicamento/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);


  angular.
  module('medicamento').
  factory('MedicamentoTotal', ['$http',
    function ($http) {
      return function () {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/medicamento/total'
        })
      }
    }
  ]);
