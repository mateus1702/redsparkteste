'use strict';

angular.
  module('medico').
  component('medicoCreate', {
     templateUrl: '/medico/medico-create/medico-create.template.html',
     controller: ['$scope', 'Medico', '$location',
       function MedicoCreateController($scope, Medico, $location) {
         $scope.medico = new Medico();
         $scope.medico.validation = {
           nomeRequiredOk: true,           
           nomeDeUsuarioRequiredOk: true,    
           senhaRequiredOk: true,   
           especialidadeOk: true 
         };

         $scope.create = function () {
           $scope.medico.validation.nomeRequiredOk = !$scope.medico.Nome ? false : true;           
           $scope.medico.validation.nomeDeUsuarioRequiredOk = !$scope.medico.NomeDeUsuario ? false : true;  
           $scope.medico.validation.senhaRequiredOk = !$scope.medico.Senha ? false : true;  
           $scope.medico.validation.especialidadeOk = !$scope.medico.Especialidade ? false : true;  

           var isValid = true;
           angular.forEach($scope.medico.validation, function(value, key) {
            isValid = isValid && value;
           });

           if (isValid) {
             $scope.loading = true;
             Medico.save($scope.medico, function() {
               $scope.loading = false;
               $location.path( "/medico" );
             });
           }
         }
       }
     ]
  });
