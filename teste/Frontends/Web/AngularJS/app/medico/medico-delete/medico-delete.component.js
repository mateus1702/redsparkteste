'use strict';

angular.
  module('medico').
  component('medicoDelete', {
     templateUrl: '/medico/medico-delete/medico-delete.template.html',
     controller: ['$scope', 'Medico', '$routeParams', '$location',
       function MedicoDeleteController($scope, Medico, $routeParams, $location) {
         $scope.loading = true;
         $scope.medico = Medico.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });

         $scope.delete = function () {
           $scope.loading = true;
           $scope.medico.$delete(function() {
             $scope.loading = false;
             $location.path( "/medico" );
           });
         }
       }
     ]
  });
