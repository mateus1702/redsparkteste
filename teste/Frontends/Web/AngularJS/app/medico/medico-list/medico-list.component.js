'use strict';

angular.
  module('medico').
  component('medicoList', {
    templateUrl: '/medico/medico-list/medico-list.template.html',
    controller: ['$rootScope', '$scope', 'Medico', 'MedicoTotal','Paging',
      function MedicoListController($rootScope, $scope, Medico, MedicoTotal, Paging) {
        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.medicos = Medico.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        MedicoTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });
      }
    ]
  });
