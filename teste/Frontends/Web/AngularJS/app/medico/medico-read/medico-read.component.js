'use strict';

angular.
  module('medico').
  component('medicoRead', {
     templateUrl: '/medico/medico-read/medico-read.template.html',
     controller: ['Medico', '$routeParams', '$scope',
       function MedicoReadController(Medico, $routeParams, $scope) {
         $scope.Id = $routeParams.Id;
         $scope.loading = true;
         $scope.medico = Medico.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });
       }
     ]
  });
