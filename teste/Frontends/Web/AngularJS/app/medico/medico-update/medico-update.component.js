'use strict';

angular.
  module('medico').
  component('medicoUpdate', {
    templateUrl: '/medico/medico-update/medico-update.template.html',
    controller: ['$scope', 'Medico', '$routeParams', '$location',
      function MedicoUpdateController($scope, Medico, $routeParams, $location) {
        $scope.loading = true;
        $scope.medico = Medico.get({ Id: $routeParams.Id }, function (response) {
          $scope.loading = false;
          $scope.medico.validation = {
            nomeRequiredOk: true,
            especialidadeOk: true
          };
        });

        $scope.update = function () {
          $scope.medico.validation.nomeRequiredOk = !$scope.medico.Nome ? false : true;
          $scope.medico.validation.especialidadeOk = !$scope.medico.Especialidade ? false : true;

          var isValid = true;
          angular.forEach($scope.medico.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            $scope.medico.$update(function () {
              $scope.loading = false;
              $location.path("/medico");
            });
          }
        }
      }
    ]
  });
