'use strict';

angular.
  module('medico').
  factory('Medico', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/medico/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);

angular.
  module('medico').
  factory('MedicoTotal', ['$http',
    function ($http) {
      return function (ConsultaId) {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/medico/total'
        });
      }
    }
  ]);
