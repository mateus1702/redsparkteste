'use strict';

angular.
  module('paciente').
  component('pacienteCreate', {
     templateUrl: '/paciente/paciente-create/paciente-create.template.html',
     controller: ['$scope', 'Paciente', '$location',
       function PacienteCreateController($scope, Paciente, $location) {
         $scope.paciente = new Paciente();
         $scope.paciente.validation = {
           nomeRequiredOk: true,           
           nomeDeUsuarioRequiredOk: true,    
           senhaRequiredOk: true,   
         };

         $scope.create = function () {
           $scope.paciente.validation.nomeRequiredOk = !$scope.paciente.Nome ? false : true;           
           $scope.paciente.validation.nomeDeUsuarioRequiredOk = !$scope.paciente.NomeDeUsuario ? false : true;  
           $scope.paciente.validation.senhaRequiredOk = !$scope.paciente.Senha ? false : true;  

           var isValid = true;
           angular.forEach($scope.paciente.validation, function(value, key) {
            isValid = isValid && value;
           });

           if (isValid) {
             $scope.loading = true;
             Paciente.save($scope.paciente, function() {
               $scope.loading = false;
               $location.path( "/paciente" );
             });
           }
         }
       }
     ]
  });
