'use strict';

angular.
  module('paciente').
  component('pacienteDelete', {
     templateUrl: '/paciente/paciente-delete/paciente-delete.template.html',
     controller: ['$scope', 'Paciente', '$routeParams', '$location',
       function PacienteDeleteController($scope, Paciente, $routeParams, $location) {
         $scope.loading = true;
         $scope.paciente = Paciente.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });

         $scope.delete = function () {
           $scope.loading = true;
           $scope.paciente.$delete(function() {
             $scope.loading = false;
             $location.path( "/paciente" );
           });
         }
       }
     ]
  });
