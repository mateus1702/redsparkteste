'use strict';

angular.
  module('paciente').
  component('pacienteList', {
    templateUrl: '/paciente/paciente-list/paciente-list.template.html',
    controller: ['$rootScope', '$scope', 'Paciente', 'PacienteTotal', 'Paging',
      function PacienteListController($rootScope, $scope, Paciente, PacienteTotal, Paging) {

        $scope.paging = Paging(function (page, qtdPerPage) {
          $scope.loading = true;
          $scope.pacientes = Paciente.query({ Page: page, QtdPerPage: qtdPerPage }, function () {
            $scope.loading = false;
          });
        });

        $scope.loading = true;
        PacienteTotal().then(function (response) {
          $scope.paging.init(response.data, $rootScope.qtdPerPage);
        });
      }
    ]
  });
