'use strict';

angular.
  module('paciente').
  component('pacienteRead', {
     templateUrl: '/paciente/paciente-read/paciente-read.template.html',
     controller: ['Paciente', '$routeParams', '$scope',
       function PacienteReadController(Paciente, $routeParams, $scope) {
         $scope.Id = $routeParams.Id;
         $scope.loading = true;
         $scope.paciente = Paciente.get({ Id: $routeParams.Id }, function(response) {
           $scope.loading = false;
         });
       }
     ]
  });
