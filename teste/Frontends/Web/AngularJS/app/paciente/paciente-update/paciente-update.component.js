'use strict';

angular.
  module('paciente').
  component('pacienteUpdate', {
    templateUrl: '/paciente/paciente-update/paciente-update.template.html',
    controller: ['$scope', 'Paciente', '$routeParams', '$location',
      function PacienteUpdateController($scope, Paciente, $routeParams, $location) {
        $scope.loading = true;
        $scope.paciente = Paciente.get({ Id: $routeParams.Id }, function (response) {
          $scope.loading = false;
          $scope.paciente.validation = {
            nomeRequiredOk: true,
          };
        });

        $scope.update = function () {
          $scope.paciente.validation.nomeRequiredOk = !$scope.paciente.Nome ? false : true;

          var isValid = true;
          angular.forEach($scope.paciente.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            $scope.paciente.$update(function () {
              $scope.loading = false;
              $location.path("/paciente");
            });
          }
        }
      }
    ]
  });
