'use strict';

angular.
  module('paciente').
  factory('Paciente', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/paciente/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);

angular.
  module('paciente').
  factory('PacienteTotal', ['$http',
    function ($http) {
      return function () {
        return $http({
          method: 'GET',
          url: 'http://localhost:5000/api/paciente/total'
        })
      }
    }
  ]);
