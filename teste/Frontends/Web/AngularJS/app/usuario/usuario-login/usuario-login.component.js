'use strict';

angular.
  module('usuario').
  component('usuarioLogin', {
    templateUrl: '/usuario/usuario-login/usuario-login.template.html',
    controller: ['$rootScope', '$scope', 'Usuario', '$location', 'UsuarioAutenticar', '$http',
      function UsuarioCreateController($rootScope, $scope, Usuario, $location, UsuarioAutenticar, $http) {
        $scope.usuario = new Usuario();
        $scope.usuario.validation = {
          nomeDeUsuarioRequiredOk: true,
          senhaRequiredOk: true,
        };

        $scope.login = function () {
          $scope.usuario.validation.nomeDeUsuarioRequiredOk = !$scope.usuario.NomeDeUsuario ? false : true;
          $scope.usuario.validation.senhaRequiredOk = !$scope.usuario.Senha ? false : true;

          var isValid = true;
          angular.forEach($scope.usuario.validation, function (value, key) {
            isValid = isValid && value;
          });

          if (isValid) {
            $scope.loading = true;
            UsuarioAutenticar($scope.usuario.NomeDeUsuario, $scope.usuario.Senha)
              .then(function successCallback(response) {
                $scope.loading = false;
                $rootScope.Usuario = response.data.Usuario;
                $http.defaults.headers.common.Authorization = `Bearer ${$rootScope.Usuario.Token}`;
                $location.path("/home");
              }, function errorCallback(response) {
                $scope.loading = false;
                if (response.status === 401) {
                  alert('Não autorizado.')
                } else if (response.status === 400) {
                  alert(response.data.message);
                }
              });
          }
        }
      }
    ]
  });
