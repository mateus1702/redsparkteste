'use strict';

angular.
  module('usuario').
  factory('Usuario', ['$resource',
    function ($resource) {
      return $resource('http://localhost:5000/api/usuario/:Id', { Id: '@Id' }, {
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      });
    }
  ]);

angular.
  module('usuario').
  factory('UsuarioAutenticar', ['$http',
    function ($http) {
      return function (NomeDeUsuario, Senha) {
        return $http({
          method: 'POST',
          data: {
            NomeDeUsuario: NomeDeUsuario,
            Senha: Senha
          },
          url: 'http://localhost:5000/api/usuario/autenticar'
        })
      }
    }
  ]);
